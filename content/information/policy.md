+++
title = "Legal Notices"
menutitle = "Legal Notices"
description = "Software licensing, community rules of conduct, website terms of use, and other legal notices."
weight = 100
pre = "<i class='fa fa-file'></i>	"
+++

## Website Terms Of Use

### Information Collection, Use, and Sharing 

Currently we are hosting our site on GitLab pages, and we have no control over what data they collect on vistors.  However, we only have access to/collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone. 

We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request..

### Your Access to and Control Over Information 

MindDrip Media neither collects nor stores any personally-identifiable information about you. All of our communication channels are "opt-in" external platforms and you may "opt-out" or leave a channel at any time following the respective platform guidelines. For addition information about how these platforms may store your information, please contact the respective platform provider.
 
### Sharing 

We may publicly share aggregate statistics about website traffic. This is not linked to any personal information that can identify any individual person.

### Cookies 

We use "cookies" on this site. A cookie is a piece of data stored on a site visitor's hard drive to help us improve your access to our site and identify repeat visitors to our site. Usage of a cookie is in no way linked to any personally identifiable information on our site.

### Links 

This web site contains links to other sites. Please be aware that we are not responsible for the content or privacy practices of such other sites. We encourage our users to be aware when they leave our site and to read the privacy statements of any other site that collects personally identifiable information.

### Other Provisions as Required by Law

Numerous other provisions and/or practices may be required as a result of laws, international treaties, or industry practices. It is up to you to determine what additional practices must be followed and/or what additional disclosures are required. Please take special notice of the California Online Privacy Protection Act (CalOPPA), which is frequently amended and now includes a disclosure requirement for “Do Not Track” signals.

If you feel that we are not abiding by this privacy policy, you should contact us immediately via email at [social@minddripmedia.com](mailto:social@minddripmedia.com).

+++
title = "Home"
MenuTitle = "Home"
chapter = false
weight = 5
pre = ""
+++

{{< banner "logo.svg" >}}

## What is MindDrip Media?
Mind Drip Media is the eventuality of many years of discussion by several Content Creators who love Linux.  We look to become a resource for Linux and Open Source focused content and to encourage and help others get started in becoming an open source evangalist.

### Core Focuses of Mind Drip
* ***Open Source First***  We strive to utilize a 100% open source workflow in the creation of our media.
* ***Regular Updates:*** We Strive to continue to produce and release Linux and Open Sourced content as often as we are able.
* ***We actually use Linux:*** We have been active users of Linux for a long time.  We have been a part of the Open Source community for many years.  
* ***Community Backed*** We hope to be 100% community backed with no sponsors.  However this is dependant on the community to join us in our mission to produce quality Linux and Open Source content.


## Recent Articles
{{< recent-articles 5 >}}

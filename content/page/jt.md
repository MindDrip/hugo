---
title: JT
subtitle: Why you'd want to hang out with me
comments: false
---
 
<img src="https://gitlab.com/MindDrip/hugo/blob/master/content/page/jt.jpg" alt="JT" width="250"/>

### My History

I was first introduced to Open Source Software by a friend who hooked me up with a machine in 94 with Slackware installed on it.  From that point on I was hooked. I bounced around between the Slackware and OpenSuSe communitues before finally finding a home within the Puppy Linux community. I became involved in the Puppy Linux Project in 2008 and have been a member of that community ever since.  I have worked in IT most of my adult life, aside from a short stint in the USAF.  In 2011, I co-founded Obsidian Security Services, a small security consulting firm providing boutique services to business clients.  In 2013, I joined the ranks of Jupiter Broadcasting as a producer for the largest and continually running Linux podcast, 'Linux Action Show' and its spin off show 'Linux Unplugged' and become the producer of BSD Now in 2015.  Most recently, I worked for iXsystems, the company behind FreeNAS - 'The World's #1 Storage OS', from 2016 up through 2018.

Having been a passionate Open Source Software community member since the mid 90s and a developer for many years, I enjoy focusing on community engagement and community support. I am highly interested in community engagement through offering community outreach, support and education as well as bug testing and feature development.


#### Open Source Community Involvement:

+ Puppy Linux Developer and Community Support: 2008-2015 http://puppylinux.com	
+ Jupiter Broadcasting - http://jupiterbroadcasting.com
 + Producer - BSD Now 2015-Current
 + Producer - Linux Action Show and Linux Unplugged 2013-2015 
 + Producer - Ask Noah Show
 + Community Moderator
    + Multiple Jupiter Broadcasting reddit communities 2014-Current
    + Jupiter Broadcasting IRC Op 2013-Current
    + Jupiter Broadcasting Mumble Moderator 2014-2017
+ YaCyPi Developer - 2014 https://www.kickstarter.com/projects/1455616350/yacypi-turnkey-raspberry-pi-based-internet-search
+ Lumina Desktop Developer, Fuzzer, and Support 2015-Current
+ Southeast LinuxFest Speaker/Staff - 2015-Current
+ CharmBug - Co-Organizer 2016-Current http://www.meetup.com/CharmBUG/
+ Lancaster Linux User group - Co-organizer 2016-Current http://www.meetup.com/lanclug/
+ OpenSource.com - Author 2016-Current https://opensource.com/users/jtpennington
+ TrueOS Developer 2016-Current
+ FreeNAS and TrueView Developer 2016-2018
+ KnoxBug - Co-Organizer 2017-Current http://knoxbug.org
+ Project Trident Developer 2018-Current
+ MeetBSD Staff - 2018


#### Conferences:

+ Linuxfest Northwest - 2014-2016
 + 2014 - Sponsor and Representing Jupiter Broadcasting
 + 2015 - Sponsor and Representing Jupiter Broadcasting
 + 2016 - Speaker, Sponsor, and Representing Jupiter Broadcasting
+ Southeast Linuxfest - 2014-2016
 + 2014 - Speaker
 + 2015 - Speaker, Sponsor, and Representing Jupiter Broadcasting
 + 2016 - Speaker, Sponsor, and Representing Jupiter Broadcasting
 + 2017 - Staff
 + 2018 - Staff, Speaker, and Sponsor
+ Ohio Linuxfest - 2014-2015
 + 2014 - Sponsor and Representing Jupiter Broadcasting
 + 2015 - General Attendee and Representitive for PCBSD Project
 + 2016 - General Attendee and Sponsor
 + 2018 - Attendee, Sponsor, and Exhibitor
+ Fosscon - 2015-2016
 + 2015 - Representitive for the Archlinux ARM Project
 + 2016 - Speaker and Representitive for the Archlinux ARM Project

+++
title = "About Us"
MenuTitle = "About Us"
chapter = false
weight = 1
pre = "<i class='fa fa-info-circle'></i>	"
+++

We are MindDrip Media:

- [Noah](https://minddrip.gitlab.io/hugo/page/noah/)
- [JT](https://minddrip.gitlab.io/hugo/page/jt/)
- [TheDude](https://minddrip.gitlab.io/hugo/page/thedude/)

{{< subpages "page/noah" >}}
{{< subpages "page/JT" >}}
